REM
REM IntToDoubleByte(IntValue AS INTEGER) AS STRING
REM Converts an INT value into a double-byte string
REM

FUNCTION IntToDoubleByte(BYREF IntValue AS INTEGER) AS STRING
  DIM AS STRING Result
  DIM AS UBYTE aByte, bByte
  bByte = INT(IntValue / 256)
  aByte = IntValue - (bByte * 256)
  Result = CHR(aByte, bByte)
  RETURN Result
END FUNCTION

REM
REM  Provides a pallette of colors for ANSI emulation
REM

DIM SHARED AnsiPallette(0 TO 255) AS UINTEGER
AnsiPallette(00) = RGB(000,000,000)   ' black
AnsiPallette(01) = RGB(187,000,000)   ' red normal
AnsiPallette(02) = RGB(000,187,000)   ' green normal
AnsiPallette(03) = RGB(187,100,000)   ' yellow normal (brown)
AnsiPallette(04) = RGB(000,000,187)   ' blue normal
AnsiPallette(05) = RGB(187,000,187)   ' magenta normal
AnsiPallette(06) = RGB(000,187,187)   ' cyan normal
AnsiPallette(07) = RGB(187,187,187)   ' white normal
AnsiPallette(08) = RGB(093,000,000)   ' red faint
AnsiPallette(09) = RGB(000,093,000)   ' green faint
AnsiPallette(10) = RGB(093,093,000)   ' yellow faint
AnsiPallette(11) = RGB(000,000,093)   ' blue faint
AnsiPallette(12) = RGB(093,000,093)   ' magenta faint
AnsiPallette(13) = RGB(000,093,093)   ' cyan faint
AnsiPallette(14) = RGB(093,093,093)   ' white faint
AnsiPallette(15) = RGB(085,085,085)   ' black bright
AnsiPallette(16) = RGB(255,085,085)   ' red bright
AnsiPallette(17) = RGB(085,255,085)   ' green bright
AnsiPallette(18) = RGB(255,255,085)   ' yellow bright
AnsiPallette(19) = RGB(085,085,255)   ' blue bright
AnsiPallette(20) = RGB(255,085,255)   ' magenta bright
AnsiPallette(21) = RGB(085,255,255)   ' cyan bright
AnsiPallette(22) = RGB(255,255,255)   ' white bright

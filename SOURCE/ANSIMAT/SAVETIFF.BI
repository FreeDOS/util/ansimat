REM
REM  Writes a TIFF file using data from BigScreenBuffer
REM

PRIVATE FUNCTION MakeTiffTAG(BYREF TagID AS INTEGER, BYREF DataType AS INTEGER, BYREF Value AS INTEGER, CountOfVal AS INTEGER = 1) AS STRING
  DIM AS STRING Result
  Result = IntToDoubleByte(TagID)                              ' TAG
  Result += IntToDoubleByte(DataType)   ' field type
  Result += IntToDWORD(CountOfVal)      ' Number of values
  Result += IntToDWORD(Value)           ' The value (or offset to data)
  RETURN Result
END FUNCTION


FUNCTION SaveTifFile(BYREF OutFile AS STRING, BYREF xsize AS INTEGER, BYREF ysize AS INTEGER) AS BYTE
  DIM AS BYTE Result = 0
  DIM AS INTEGER TiffFileHandler, x, y, PackBitDataSize
  DIM AS UBYTE RleValue, OldPixel
  TiffFileHandler = FREEFILE

  OPEN OutFile FOR BINARY AS #TiffFileHandler
  REM Write TIFF header
  PUT #TiffFileHandler,, "II"         ' Little endian byte order marker
  PUT #TiffFileHandler,, CHR(42, 0)   ' TIFF magic bytes
  PUT #TiffFileHandler,, CHR(8,0,0,0) ' Offset of the first IFD directory (must be a WORD boundary! Even offset)

  REM IFD struct:
  PUT #TiffFileHandler,, CHR(9,0) ' number of entries in the directory

  PUT #TiffFileHandler,, MakeTiffTAG(256, 3, xsize)  ' TAG=ImageWidth, datatype=SHORT, value=ysize
  PUT #TiffFileHandler,, MakeTiffTAG(257, 3, ysize)  ' TAG=ImageLength, datatype=SHORT, value=xsize
  PUT #TiffFileHandler,, MakeTiffTAG(258, 3, 8)      ' TAG=BitsPerSample, datatype=SHORT, value=8
  PUT #TiffFileHandler,, MakeTiffTAG(259, 3, 32773)      ' TAG=Compression, datatype=SHORT, value=32773 (PackBits)
  PUT #TiffFileHandler,, MakeTiffTAG(262, 3, 3)      ' TAG=PhotometricInterpretation, datatype=SHORT, value=3 (Palette color)
  PUT #TiffFileHandler,, MakeTiffTAG(273, 3, (14+9*12) + (256*6))  ' TAG=StripOffset, datatype=SHORT, value=offset of the strip
  PUT #TiffFileHandler,, MakeTiffTAG(278, 3, xsize)  ' TAG=RowsPerStrip, datatype=SHORT, value=xsize (entire image is in one strip)
  PUT #TiffFileHandler,, MakeTiffTAG(279, 4, xsize*ysize)  ' TAG=StripByteCount, datatype=LONG (4bytesValue), value=xsize*ysize
 'MakeTiffTAG(282, 5, 300)   ' TAG=XResolution, datatype=RATIONAL (2x4bytes), value=300
 'MakeTiffTAG(283, 5, 300)   ' TAG=YResolution, datatype=RATIONAL (2x4bytes), value=300
 'MakeTiffTAG(296, 3, 2)     ' TAG=ResolutionUnit, datatype=SHORT, value=2 (inch)
  PUT #TiffFileHandler,, MakeTiffTAG(320, 3, (14+9*12) - 1, 768)  ' TAG=ColorMap, datatype=SHORT, value=offset of the color table count=768
  PUT #TiffFileHandler,, CHR(0,0,0,0)   ' IFD trailer - offset of next IFD - must be at word boundary (even offset) (0 for the last)

  REM ColorMap
  FOR x = 0 TO 255
    PUT #TiffFileHandler,, IntToDoubleByte(((AnsiPallette(x) SHR 16) AND &b11111111) * 257)
  NEXT x
  FOR x = 0 TO 255
   PUT #TiffFileHandler,, IntToDoubleByte(((AnsiPallette(x) SHR 8) AND &b11111111) * 257)
  NEXT x
  FOR x = 0 TO 255
    PUT #TiffFileHandler,, IntToDoubleByte(((AnsiPallette(x)) AND &b11111111) * 257)
  NEXT x
  REM Image data now
  FOR y = 0 TO ysize - 1
    OldPixel = BigScreenBuffer(0, y)
    RleValue = 1
    FOR x = 1 TO xsize - 1
      IF BigScreenBuffer(x,y) = OldPixel AND RleValue < 128 THEN
          RleValue += 1
        ELSE
          PUT #TiffFileHandler,, CHR((((RleValue-1) XOR &b11111111) + 1)) ' Write RLE value (as two-complement - 1)
          PUT #TiffFileHandler,, CHR(OldPixel)
          RleValue = 1
          OldPixel = BigScreenBuffer(x,y)
          PackBitDataSize += 2
      END IF
    NEXT x
    PackBitDataSize += 2
    PUT #TiffFileHandler,, CHR((((RleValue-1) XOR &b11111111) + 1)) ' Write RLE value (as two-complement - 1)
    PUT #TiffFileHandler,, CHR(OldPixel)
  NEXT y

  PUT #TiffFileHandler, 95, MakeTiffTAG(279, 4, PackBitDataSize)  ' Update the StripByteCount value

  CLOSE #TiffFileHandler
  RETURN Result
END FUNCTION

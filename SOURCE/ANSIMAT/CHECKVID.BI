REM
REM CheckVideoRes(xres AS INTEGER, yres AS INTEGER, colordepth AS INTEGER) AS BYTE
REM
REM Returns 1 if the given resolution is supported by the video system, 0 otherwise.
REM
REM Copyright (C) Mateusz Viste
REM Last update: 17 Jul 2010
REM

FUNCTION CheckVideoRes(BYREF xrez AS INTEGER, BYREF yrez AS INTEGER, BYREF colordepthrez AS INTEGER) AS BYTE
  DIM AS INTEGER VideoMode
  DIM AS BYTE VideoModeOk = 0
  VideoMode = SCREENLIST(colordepthrez)
  DO
    IF HIWORD(VideoMode) = xrez AND LOWORD(VideoMode) = yrez THEN VideoModeOk = 1
    VideoMode = ScreenList()
  LOOP UNTIL VideoMode = 0
  RETURN VideoModeOk
END FUNCTION

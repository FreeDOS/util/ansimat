REM
REM Part of the ANSiMat project
REM Copyright (C) Mateusz Viste 2010
REM

SUB EnableDraftMode()
  IF DraftModeFlag = 0 THEN DraftModeFlag = -1
  DraftModeStartTime = TIMER
END SUB

REM
REM Returns 1 if the file/dir exists, 0 otherwise
REM Author: Mateusz Viste
REM Last update: 17 Jul 2010
REM

FUNCTION fexists(filename As String) AS BYTE
  IF LEN(DIR(filename, 1 OR 2 OR 4 OR 16 OR 32)) > 0 THEN
      RETURN 1
    ELSE
      RETURN 0
  END IF
END FUNCTION
